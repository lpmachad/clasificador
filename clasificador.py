#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Desarrolla un programa que permita ingresar una cadena
(un string) y evalúe si es un correo electrónico, un entero,
un real u otra cosa.
"""

import sys

# Función que verifica si la cadena es un correo electrónico
def es_correo_electronico(string):
    if string.count("@") != 1:  # Verifica si hay exactamente un "@" en la cadena
        return False

    usuario, dominio = string.split("@")  # Divide la cadena en usuario y dominio

    if "." not in dominio:  # Verifica si hay al menos un "." en el dominio
        return False

    if not usuario or not dominio:  # Verifica si tanto usuario como dominio no están vacíos
        return False

    return True

# Función que verifica si la cadena es un entero
def es_entero(string):
    try:
        int(string)  # Intenta convertir la cadena a un entero
        return True
    except ValueError:
        return False

# Función que verifica si la cadena es un número real
def es_real(string):
    try:
        float(string)  # Intenta convertir la cadena a un número real
        return True
    except ValueError:
        return False

# Función que evalúa la entrada y devuelve un mensaje descriptivo
def evaluar_entrada(string):
    if es_correo_electronico(string):
        return 'Es un correo electrónico.'
    elif es_entero(string):
        return 'Es un entero.'
    elif es_real(string):
        return 'Es un número real.'
    elif string == "":
        return None
    else:
        return 'No es ni un correo, ni un entero, ni un número real.'

# Función principal del script
def main():
    if len(sys.argv) < 2:
        sys.exit("Error: se espera al menos un argumento")
    string = sys.argv[1]
    resultado = evaluar_entrada(string)
    print(resultado)

# Ejecuta el script si es el programa principal
if __name__ == '__main__':
    main()

